import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import OTPFrontend from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<OTPFrontend />, document.getElementById('root'));

serviceWorker.unregister();
