import React from 'react';
import './App.css';

class RenderForm extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state=
		{
			inputFields:
			{
				sourceBuf: "",
				destinationBuf1: "",
				destinationBuf2: ""
			},
			results: {},
			encryptionVisible: true
		};

		this.handleSourceBufChange=this.handleSourceBufChange.bind(this);
		this.handleDestinationBufChange=this.handleDestinationBufChange.bind(this);
	}

	handleSourceBufChange(e)
	{
		e.preventDefault();

		var sourceBuf=e.target.value;

		var encodedSourceBuf=encodeURIComponent(Buffer.from(sourceBuf).toString("base64"));

		fetch("http://localhost:3000/encrypt?sourceBuf="+encodedSourceBuf)
			.then(response => response.json())
			.then(buf => this.setState({inputFields: {sourceBuf: sourceBuf, destinationBuf1: buf.destinationBuf1, destinationBuf2: buf.destinationBuf2}}));
	}

	handleDestinationBufChange(e)
	{
		e.preventDefault();

		var inputFields=this.state.inputFields;

		inputFields[e.target.name]=e.target.value;

		fetch("http://localhost:3000/decrypt?sourceBuf1="+inputFields.destinationBuf1+"&sourceBuf2="+inputFields.destinationBuf2)
			.then(response => response.json())
			.then(buf => this.setState({inputFields: {sourceBuf: Buffer.from(decodeURIComponent(buf.destinationBuf), "base64").toString("binary"), destinationBuf1: inputFields.destinationBuf1, destinationBuf2: inputFields.destinationBuf2}}));
	}

	render()
	{
		var sourceBufStyle=
		{
			width: "454px",
			height: "192px",
		},
		destinationBuf1Style=
		{
			width: "224px",
			height: "192px",
		},
		destinationBuf2Style=
		{
			width: "224px",
			height: "192px",
		},
		panel="";

		panel=
					<div>
							<form onSubmit={e=>e.preventDefault()}>
								<textarea name='sourceBuf' value={this.state.inputFields["sourceBuf"]} onChange={this.handleSourceBufChange} style={sourceBufStyle} placeholder='Message to be encrypted'/>
								<textarea name='destinationBuf1' value={this.state.inputFields["destinationBuf1"]} onChange={this.handleDestinationBufChange} style={destinationBuf1Style} placeholder='Key'/>
								<textarea name='destinationBuf2' value={this.state.inputFields["destinationBuf2"]} onChange={this.handleDestinationBufChange} style={destinationBuf2Style} placeholder='Encrypted Message'/>
							</form>
						</div>;

		return panel;
	}
}

class OTPFrontend extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state=
		{
		};
	}

	render()
	{
		var style1=
		{
			backgroundColor: "rgb(192, 192, 192)",
			width: "512px",
			height: "512px",
			borderRadius: "32px",
			padding: "32px",
			boxShadow: "32px 32px 32px rgba(0, 0, 0, 192)",
			margin: "32px auto"
		},
		style2=
		{
			width: "512px",
			height: "32px",
		};

		return <div style={style1}><div><h1>OTP Encryption Server 1.0</h1></div><div style={style2}>Copyright 2020 by Julian Meinold</div><RenderForm/></div>;
	}
}

export default OTPFrontend;
